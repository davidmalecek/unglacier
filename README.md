# Set of scripts used to unarchive demo files from Amazon Glacier to S3

This is a deprecated repository. Please see https://bitbucket.org/RTSmunity/unglacier-demo-files/src/master/ instead.
