@Grab("com.amazonaws:aws-java-sdk-s3:1.11.505")
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.GlacierJobParameters
import com.amazonaws.services.s3.model.ListObjectsV2Request
import com.amazonaws.services.s3.model.RestoreObjectRequest
import com.amazonaws.services.s3.model.Tier

def s3Client = AmazonS3ClientBuilder.standard()
        .withCredentials(new ProfileCredentialsProvider())
        .withRegion('eu-west-1')
        .build()

def request = new ListObjectsV2Request().withBucketName('rts-dota2proscraper-demos')

def listing = s3Client.listObjectsV2(request)
def objectSummaries = listing.getObjectSummaries()

while (listing.isTruncated()) {

    request.setContinuationToken(listing.getNextContinuationToken())

    listing = s3Client.listObjectsV2(request)
    objectSummaries.addAll(listing.getObjectSummaries())
}

def theDate = new Date().parse('yyyy/MM/dd', '2018/12/01')

def glacierObjectsToBeRestored = objectSummaries
        .findAll { it.storageClass == 'GLACIER' }
        .findAll { it.lastModified >= theDate }

println "all s3 objects size: $objectSummaries.size"
println "glacier objects size: $glacierObjectsToBeRestored.size"

// save glacier objects' metadata as csv
new File("restored-glacier-objects-${System.currentTimeMillis()}.csv").withWriter { writer ->
  writer.writeLine 'BucketName,Key,Size,LastModified,StorageClass'
  glacierObjectsToBeRestored
    .toSorted { it.lastModified }
    .each { writer.writeLine "$it.bucketName,$it.key,$it.size,$it.lastModified,$it.storageClass" }
}

int restoreForDays = 2
glacierObjectsToBeRestored.each {
    def restoreRequest = new RestoreObjectRequest(it.bucketName, it.key, restoreForDays)
    restoreRequest.setGlacierJobParameters(new GlacierJobParameters(tier: Tier.Bulk))
    s3Client.restoreObjectV2(restoreRequest)

    def restoreResponse = s3Client.getObjectMetadata(it.bucketName, it.key)
    println "$it.key restore requested, status: ${restoreResponse.getOngoingRestore()}"
}
