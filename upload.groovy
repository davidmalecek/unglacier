@Grab("com.amazonaws:aws-java-sdk-s3:1.11.505")
import com.amazonaws.AmazonServiceException
import com.amazonaws.SdkClientException
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.PutObjectRequest

import java.io.File
import java.nio.file.Paths
import java.nio.file.Files

if (this.args.length < 1) {
  println "usage: groovy upload.groovy [input-directory]"
  return 1
}

def inputDirectory = Paths.get(this.args[0])

if (!Files.isDirectory(inputDirectory)) {
  println "The directory ${outputDirectory} does not exist!"
  return 1
}

def s3Client = AmazonS3ClientBuilder.standard()
        .withRegion('eu-west-1')
        .withCredentials(new ProfileCredentialsProvider())
        .build()

def bucketName = 'rts-dota2proscraper-jsons'

inputDirectory.toFile().eachFileMatch(~/.*.dem.10.json/) { file ->
  println "uploading '$file.name' to S3"
  PutObjectRequest request = new PutObjectRequest(bucketName, file.name, file)
  ObjectMetadata metadata = new ObjectMetadata()
  metadata.setContentType('application/json')
  request.setMetadata(metadata)
  s3Client.putObject(request)
}
